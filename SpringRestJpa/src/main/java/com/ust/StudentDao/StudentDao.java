package com.ust.StudentDao;

import com.ust.entity.Student;

public interface StudentDao {
	Student getStudent(int id) throws Exception;

	//Student createStudent() throws Exception;
}
