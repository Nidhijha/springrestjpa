package com.ust.StudentDao;

import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.Persistence;

import com.ust.entity.Student;
import com.ust.util.JpaUtil;

public class StudentDaoJpaImpl implements StudentDao{

	@Override
	public Student getStudent(int id) throws Exception {
		EntityManager em = JpaUtil.getEntityManager();
		Student std = em.find(Student.class,id);
		return std;

}
//	public static void main(String[] args) {
//		try {
//			new StudentDaoJpaImpl().createStudent();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//		
//	public Student createStudent() throws Exception {
//		EntityManagerFactory emf = Persistence.createEntityManagerFactory("studentity");
//		EntityManager em = emf.createEntityManager();
//		em.getTransaction().begin();
//		Student std  = new Student();
//		String name = "Jishnu";
//		long mobile = 987612345l;
//		String subject = "SpringBoot";
//		std.setName(name);
//		std.setMobile(mobile);
//		std.setSubject(subject);
//		em.persist(std);
//		em.getTransaction().commit();
//		em.close();
//		return std;
//	}
}
