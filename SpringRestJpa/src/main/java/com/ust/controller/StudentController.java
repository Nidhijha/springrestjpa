package com.ust.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.ust.StudentDao.StudentDao;
import com.ust.StudentDao.StudentDaoJpaImpl;
import com.ust.entity.Student;

@RestController
public class StudentController {
private StudentDao sDao;
	public StudentController() {
		sDao = new StudentDaoJpaImpl();
	}
	@GetMapping(value = "/std/greet")
	public String sayHello() {
		return "Hello there....";
	}
	@GetMapping("/std/find/{id}")
	public Student findStudent(@PathVariable int id) throws Exception{
		System.out.println("id = "+ id);
		Student std = sDao.getStudent(id);
		return std;
	}
}