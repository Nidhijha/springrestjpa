package com.ust.exception;

public class StudentException extends Exception{

	public StudentException(String msg) {
		super(msg);
	}
	
}
