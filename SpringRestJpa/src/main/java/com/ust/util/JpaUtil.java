package com.ust.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.ust.exception.StudentException;

public class JpaUtil {
	private static EntityManagerFactory emf;
	private static EntityManager em;

	public static EntityManager getEntityManager() throws Exception{
		try {
			if (em == null || !em.isOpen()) {
				emf = Persistence.createEntityManagerFactory("studentity");
				em = emf.createEntityManager();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new StudentException("Error Creating Entity Manager");
		}
		return em;
	}
	public static void close() {
		if(em!=null) em.close();
	}

}
